Package.describe({
  name: 'howcloud:react',
  summary: "React library on client and server for meteor apps (as well as requirement of build process to support it)"
});

var reactVersion = "0.14.2";

Npm.depends({
  "react": reactVersion,
  "react-dom": reactVersion
});

Package.on_use(function(api) {

  // set package dependencies
  api.use('howcloud:react-build');
  // api.use('cscottnet:es5-shim'); // for ie8 [todo: only load this conditionally if ie8]
                                    // we are not bothering with IE8 compat for now - loads of other stuff apart from react would need it including dealing with the fact onScroll does not work

  // Choose which version of react we are going to bundle up depending on if we are in product or development mode
  // NOTE: this must be explicitly set on the server environment by exporting export NODE_ENV=development

  if (process.env.NODE_ENV === "development") {
    api.add_files("vendor/react-" + reactVersion + "-dev.js", "client");
  } else {
    api.add_files("vendor/react-" + reactVersion + ".js", "client");
  }

  // On the server, we use the modules that ship with react.
  api.add_files("src/require-react.js", "server");
  api.export("React", "server");
  api.export("ReactDOMServer", "server");

  // Shared export of refs for mixins etc
  api.add_files("src/addonRefs.js", ["client", "server"]);
  api.export("ReactCSSTransitionGroup", ["server", "client"]);
  api.export("ReactTransitionGroup", ["server", "client"]);

});
